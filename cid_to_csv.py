from pubchempy import Compound
import pandas as pd
import numpy as np
from tqdm import tqdm

# stuff
df = pd.read_pickle("data/Compound_Properties.pck1")
cids = df.CID[df.CID.notnull()].unique()

columns = ['cid','canonical_smiles','molecular_weight','xlogp']
compounds = pd.DataFrame(columns=columns, index=range(len(cids)))
for i in tqdm(range(len(cids))):
    try:
        c = Compound.from_cid(cids[i])
        for column in columns:
            compounds[column][i] = c.__getattribute__(column)
    except:
        for column in columns:
            compounds[column][i] = None
    compounds.to_pickle('data/compounds.pck')
compounds.to_csv('data/compounds.csv')